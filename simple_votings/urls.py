"""simple_votings URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include

from simple_votings import settings
from vote import views
from vote.views import get_menu_context

urlpatterns = [
    path('', views.index_page, name='index'),

    path('user/', include([
        path('admin/', admin.site.urls),
        path('login/', auth_views.LoginView.as_view(
            extra_context={'menu': get_menu_context(), 'pagename': 'Авторизация'}), name='user.login'),
        path('logout/', auth_views.LogoutView.as_view(), name='user.logout'),
        path('id<int:id>/', views.user, name='user.current'),
        path('registration/', views.registration, name='user.registration'),

        path('password-reset/', auth_views.PasswordResetView.as_view(
            extra_context={'menu': get_menu_context(), 'pagename': 'Сброс пароля'},
            template_name='registration/password/password_reset_form.html',
            email_template_name='registration/password/password_reset_email.html',
        ), name='password_reset'),
        path('password-reset/done/', auth_views.PasswordResetView.as_view(
            extra_context={'menu': get_menu_context(), 'pagename': 'Сброс пароля'},
            template_name='registration/password/password_reset_done.html'
        ), name='password_reset_done'),
        path('password-reset/complete/', auth_views.PasswordResetCompleteView.as_view(
            extra_context={'menu': get_menu_context(), 'pagename': 'Сброс пароля'},
            template_name='registration/password/password_reset_complete.html'
        ), name='password_reset_complete'),
        path('password-reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
            extra_context={'menu': get_menu_context(), 'pagename': 'Сброс пароля'},
            template_name='registration/password/password_reset_confirm.html'
        ), name='password_reset_confirm'),

        path('profile/', include([
            path('', views.profile, name='profile'),
            path('password-change/', auth_views.PasswordChangeView.as_view(
                extra_context={'menu': get_menu_context(), 'pagename': 'Изменение пароля'},
                template_name='registration/password/password_change_form.html'
            ), name='password_change'),
        ])),
    ])),

    path('voting/', include([
        path('id<int:id>/', views.voting, name='voting.current'),
        path('list/', views.voting_list_page, name='voting.list'),
        path('new/', views.new_voting_page, name='voting.new'),
        path('edit/id<int:id>/', views.edit_voting_page, name='voting.edit'),
    ])),

    path('report/', include([
        path('new_report/', views.new_report_page, name='report.new'),
    ])),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
