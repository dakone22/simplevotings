function gen_voting_chart(labels, values){
    var ctx = document.getElementById('voting_chart').getContext('2d');
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    function get_colors(data){
        colors = []
        data.forEach(function(item, i, data) { colors.push(getRandomColor()); })
        return colors;
    }

    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                data: values,
                backgroundColor: get_colors(values),
                borderColor: 'rgba(0, 0, 0, 0)',
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                position: 'left'
            },
            responsive: true,
        }
    });
}
