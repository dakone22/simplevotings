from os.path import join

from django.contrib import auth
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView

from vote.utils import get_icon_html, truncate


def _get_url(url):
    def func(self):
        return reverse(url, args=[str(self.id)])

    return func


def _get_html(self, style="text-info", max_len=0):
    return '<a class="{style}" href="{url}">{text}</a>'.format(style=style, url=self.get_url(),
                                                               text=truncate(str(self), max_len))


auth.models.User.add_to_class('get_url', _get_url('user.current'))
auth.models.User.add_to_class('get_html', _get_html)


# ========================= Profile model =========================
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(verbose_name="Аватар", upload_to=join('user_avatars', 'uploads/'),
                               default=join('user_avatars', 'no-image.jpg'), blank=True, null=True)
    biography = models.TextField(verbose_name="О себе", max_length=5000)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# ========================= Voting models =========================
# for i in range(50): Voting.objects.create(author=User.objects.all()[0], is_open=True, date=timezone.now(), title="TITLE{}".format(i), text='TEXTEXT{}'.format(i), author_visible=0, voting_users_visible=0, result_visible=0)
# Voting.objects.create(author=User.objects.all()[0], is_open=True, date=timezone.now(), title=title, text=text, author_visible=0, voting_users_visible=0, result_visible=0)
class Voting(models.Model):
    author = models.ForeignKey(verbose_name="Автор", to=User, on_delete=models.CASCADE)

    voting_status = (
        (0, 'Закрыто'),
        (1, 'Активно'),
        (2, 'Закончено'),
    )

    status = models.IntegerField(verbose_name='Статус голосования', choices=voting_status)
    date = models.DateTimeField(verbose_name='Время создания')
    title = models.CharField(verbose_name="Заголовок", max_length=100)
    text = models.TextField(verbose_name="Содержание", max_length=5000)

    maximum_votes = models.IntegerField(verbose_name="Лимит голосов", default=100,
                                        validators=[MinValueValidator(0), MaxValueValidator(100)])

    secure_level = (
        (0, 'Видно всем'),
        (1, 'Видно только зарегистрированным пользователям'),
        (2, 'Видно только автору (и администрации)'),
    )

    author_visible = models.IntegerField(verbose_name="Видимость автора", choices=secure_level, default=0)
    voting_users_visible = models.IntegerField(verbose_name="Видимость голосующих", choices=secure_level, default=0)
    result_visible = models.IntegerField(verbose_name="Видимость результатов", choices=secure_level, default=0)

    def get_voted_users(self):
        users = set()
        for item in VotingItem.objects.filter(voting=self):
            for fov in FactOfVoting.objects.filter(voting_item=item):
                users.add(fov.user)
        return list(users)

    def __str__(self):
        return self.title

    @property
    def current_votes(self):
        return len(self.get_voted_users())

    @property
    def current_votes_percent(self):
        if not self.maximum_votes: return 0
        return round(self.current_votes / self.maximum_votes * 100)

    @property
    def is_open(self):
        return self.status == 1

    @property
    def type(self):
        return VotingItem.objects.filter(voting=self)[0].type


Voting.add_to_class('get_html', _get_html)
Voting.add_to_class('get_url', _get_url('voting.current'))


def check_voting(voting):
    if voting.maximum_votes <= 0: return
    if voting.current_votes >= voting.maximum_votes:
        voting.status = 2
        voting.save()

        for u in voting.get_voted_users():
            if u != voting.author:
                end_voting_notification(user=u, voting=voting)
        end_voting_author_notification(voting=voting)


class VotingItem(models.Model):
    voting = models.ForeignKey(verbose_name="Голосование", to=Voting, on_delete=models.CASCADE)
    text = models.TextField(verbose_name="Содержание", max_length=50)

    voting_type = (
        (0, 'Radio Button'),
        (1, 'Check Box'),
    )

    type = models.IntegerField(verbose_name="Тип вопроса", choices=voting_type)

    def is_radio(self):
        return self.type == 0

    def is_combo(self):
        return self.type == 1

    def get_type(self):
        return 'checkbox' if self.type == 1 else 'radio'


class FactOfVoting(models.Model):
    user = models.ForeignKey(verbose_name="Автор", to=User, on_delete=models.CASCADE)
    voting_item = models.ForeignKey(verbose_name="Голосование", to=VotingItem, on_delete=models.CASCADE)
    date = models.DateTimeField(verbose_name='Время голосования')

    @property
    def voting(self):
        return self.voting_item.voting


class VotingList(ListView):
    model = Voting


class FactOfVotingList(ListView):
    model = FactOfVoting


# ========================= Other models =========================
class Comment(models.Model):
    user = models.ForeignKey(verbose_name="Автор", to=User, on_delete=models.CASCADE)
    voting = models.ForeignKey(verbose_name="Голосование", to=Voting, on_delete=models.CASCADE)
    date = models.DateTimeField(verbose_name='Время создания')
    text = models.TextField(verbose_name="Содержание", max_length=150)


class Report(models.Model):
    user = models.ForeignKey(verbose_name="Автор", to=User, on_delete=models.CASCADE)
    voting = models.ForeignKey(verbose_name="Голосование", to=Voting, on_delete=models.CASCADE)

    report_status = (
        (0, 'В очереди'),
        (1, 'Обработка'),
        (2, 'Закрыто'),
    )

    status = models.IntegerField(verbose_name="Статус", choices=report_status, default=0)
    date = models.DateTimeField(verbose_name='Время создания')
    text = models.TextField(verbose_name="Содержание", max_length=300)


class ReportList(ListView):
    model = Report


class Notification(models.Model):
    user = models.ForeignKey(verbose_name="Пользователь", to=User, on_delete=models.CASCADE)
    voting = models.ForeignKey(verbose_name="Голосование", to=Voting, on_delete=models.CASCADE)

    notification_status = (
        (0, 'Непрочитанное'),
        (1, 'Прочитанное'),
    )

    status = models.IntegerField(verbose_name="Статус", choices=notification_status, default=0)
    date = models.DateTimeField(verbose_name='Время получения')
    html_text = models.TextField(verbose_name="Содержание", max_length=150)
    icon = models.CharField(verbose_name="Иконка", max_length=150, blank=True, default="")


def vote_notification(user, voting):
    ico = 'check-' + ('box' if voting.type == 1 else 'circle')

    if user == voting.author:
        text = 'Вы проголосовали в своём голосовании «{v}».'.format(v=voting.get_html(), )
    else:
        text = 'Вы проголосовали в голосовании «{v}» от {u}.'.format(v=voting.get_html(), u=user.get_html())

    Notification.objects.create(user=user, voting=voting, date=timezone.now(), html_text=text, icon=ico)


def new_voting_notification(voting):
    ico = 'list-' + ('check' if voting.type == 1 else 'ul')

    text = 'Вы создали голосование «{v}».'.format(v=voting.get_html())
    Notification.objects.create(user=voting.author, voting=voting, date=timezone.now(), html_text=text, icon=ico)


def end_voting_notification(user, voting):
    ico = 'x-octagon'
    text = 'Голосование «{v}» от {u}, в котором Вы голосовали, закончено.'.format(
        v=voting.get_html(), u=user.get_html())
    Notification.objects.create(user=user, voting=voting, date=timezone.now(), html_text=text, icon=ico)


def end_voting_author_notification(voting):
    ico = 'x-octagon'
    text = 'Ваше голосование «{v}» закрыто.'.format(v=voting.get_html())
    Notification.objects.create(user=voting.author, voting=voting, date=timezone.now(), html_text=text, icon=ico)
