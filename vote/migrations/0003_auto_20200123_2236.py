# Generated by Django 3.0.2 on 2020-01-23 19:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vote', '0002_auto_20200121_1812'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voting',
            name='author_visible',
            field=models.IntegerField(choices=[(0, 'Видно всем'), (1, 'Видно только зарегистрированным пользователям'), (2, 'Видно только автору (и администрации)')], default=0, verbose_name='Видимость автора'),
        ),
        migrations.AlterField(
            model_name='voting',
            name='result_visible',
            field=models.IntegerField(choices=[(0, 'Видно всем'), (1, 'Видно только зарегистрированным пользователям'), (2, 'Видно только автору (и администрации)')], default=0, verbose_name='Видимость результатов'),
        ),
        migrations.AlterField(
            model_name='voting',
            name='voting_users_visible',
            field=models.IntegerField(choices=[(0, 'Видно всем'), (1, 'Видно только зарегистрированным пользователям'), (2, 'Видно только автору (и администрации)')], default=0, verbose_name='Видимость голосующих'),
        ),
    ]
