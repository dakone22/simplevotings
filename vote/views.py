from os.path import join

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import SetPasswordForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils import timezone

from vote.forms import NewReportForm, RegistrationForm, UserNameForm, NewVotingForm, VotingItemFormset, get_vote_form, \
    AvatarForm, BiographyForm, ProfileForm
from vote.models import Voting, VotingItem, FactOfVoting, Report, Notification, vote_notification, \
    new_voting_notification, check_voting


def get_menu_context():
    return {
        'auth': {
            'menu': [
                {'url_name': 'index', 'name': 'Главная'},
                {'url_name': 'voting.list', 'name': 'Активные голосования'},
                {'url_name': 'voting.new', 'name': 'Новое голосование'},
            ],
            'user_menu': [
                {'url_name': 'profile', 'name': 'Профиль'},
                {'url_name': 'user.logout', 'name': 'Выйти'},
            ],
        },
        'unauth': {
            'menu': [
                {'url_name': 'index', 'name': 'Главная'},
                {'url_name': 'voting.list', 'name': 'Активные голосования'},
            ],
            'user_menu': [
                {'url_name': 'user.login', 'name': 'Войти'},
                {'url_name': 'user.registration', 'name': 'Регистрация'},
            ],
        }
    }


def index_page(request):
    context = {
        'pagename': 'Главная',
        'menu': get_menu_context()
    }
    return render(request, join('pages', 'index.html'), context)


@login_required
def profile(request):
    context = {
        'pagename': 'Профиль – {}'.format(request.user),
        'menu': get_menu_context()
    }

    already_votings = set([fov.voting_item.voting for fov in FactOfVoting.objects.filter(user=request.user)])
    created_votings = [v for v in Voting.objects.filter(author=request.user)]

    context['notifications'] = Notification.objects.filter(user=request.user).order_by('-date')
    context['unread_count'] = len(context['notifications'].filter(status=0))
    context['votings'] = already_votings
    context['created_votings'] = created_votings
    context['vote_count'] = len(already_votings)
    context['voting_count'] = len(created_votings)
    context['reports'] = list(reversed(Report.objects.filter(user=request.user).order_by('date')))

    name_form = UserNameForm(instance=request.user)
    password_form = PasswordChangeForm(request.user)
    avatar_form = AvatarForm(instance=request.user.profile)
    biography_form = BiographyForm(instance=request.user.profile)
    if request.method == "POST":
        if 'name_submit' in request.POST:
            name_form = UserNameForm(request.POST, instance=request.user)
            if name_form.is_valid():
                name_form.save()
                messages.success(request, 'Вы успешно обновили имя.')
                return redirect(reverse('profile'))
        elif 'password_submit' in request.POST:
            password_form = PasswordChangeForm(request.user, request.POST)
            if password_form.is_valid():
                password_form.save()
                messages.success(request, 'Вы успешно изменили пароль.')
                return redirect(reverse('profile'))
        elif 'avatar_submit' in request.POST:
            avatar_form = AvatarForm(request.POST, request.FILES, instance=request.user.profile)
            if avatar_form.is_valid():
                avatar_form.save()
                messages.success(request, 'Вы успешно обновили аватар.')
                return redirect(reverse('profile'))
        elif 'biography_submit' in request.POST:
            biography_form = BiographyForm(request.POST, instance=request.user.profile)
            if biography_form.is_valid():
                biography_form.save()
                messages.success(request, 'Вы успешно изменили имя.')
                return redirect(reverse('profile'))

    context['name_form'] = name_form
    context['password_form'] = password_form
    context['avatar_form'] = avatar_form
    context['biography_form'] = biography_form

    return render(request, join('registration', 'profile', 'profile-base.html'), context)


def registration(request):
    context = {
        'pagename': 'Регистрация',
        'menu': get_menu_context(),
    }

    if request.method == "POST":
        u = User()
        user_form = RegistrationForm(request.POST, instance=u)
        password_form = SetPasswordForm(u, request.POST)
        profile_form = ProfileForm(request.POST)

        if user_form.is_valid() and password_form.is_valid() and profile_form.is_valid():
            user_form.save()
            password_form.save()
            u.save()
            profile_form2 = ProfileForm(request.POST, request.FILES, instance=u.profile)
            profile_form2.save()

            messages.success(request, 'Вы успешно зарегестрировались.')

            return redirect(reverse('user.login'))
    else:
        user_form = RegistrationForm()
        password_form = SetPasswordForm(None)
        profile_form = ProfileForm()

    context['user_form'] = user_form
    context['password_form'] = password_form
    context['profile_form'] = profile_form

    return render(request, join('registration', 'registration.html'), context)


def user(request, id):
    context = {
        'pagename': 'Пользователь не найден',
        'menu': get_menu_context(),
        'found': False
    }

    user_list = User.objects.filter(id=id)
    if user_list:
        user = user_list[0]
        context['user'] = user
        context['found'] = True
        context['pagename'] = str(user)
        fovs = list(reversed(FactOfVoting.objects.filter(user=user).order_by('date')))
        already_votings = set([fov.voting_item.voting for fov in fovs])
        created_votings = [v for v in Voting.objects.filter(author=user)]

        context['fovs'] = fovs

        fovs_uniq = []
        _fovs_uniq_votings = []
        for fov in fovs:
            if fov.voting not in _fovs_uniq_votings:
                fovs_uniq.append(fov)
                _fovs_uniq_votings.append(fov.voting)
        context['fovs_uniq'] = fovs_uniq
        context['votings'] = already_votings
        context['created_votings'] = created_votings
        context['vote_count'] = len(already_votings)
        context['voting_count'] = len(created_votings)

    return render(request, join('registration', 'profile', 'user.html'), context)


def get_page_object(request_GET_get_page, objects, count_on_list=5, adjacent_pages=2):
    paginator = Paginator(objects, count_on_list)
    page_obj = paginator.get_page(request_GET_get_page)

    start_page = max(page_obj.number - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1
    end_page = page_obj.number + adjacent_pages + 1
    if end_page >= paginator.num_pages - 1:
        end_page = paginator.num_pages + 1

    page_numbers = [n for n in range(start_page, end_page) if n in range(1, paginator.num_pages + 1)]

    page_obj.page_numbers = page_numbers
    page_obj.show_first = 1 not in page_numbers
    page_obj.show_last = paginator.num_pages not in page_numbers

    return page_obj


def voting_list_page(request):
    context = {
        'pagename': 'Активные голосования',
        'menu': get_menu_context()
    }

    votings_list = list(reversed(Voting.objects.filter(status=1).order_by('date')))

    context['page_obj'] = get_page_object(request.GET.get('page'), votings_list, 5, 2)

    return render(request, join('pages', 'voting-list.html'), context)


def can_be_edited(voting: Voting):
    return not voting.current_votes


def voting(request, id):
    context = {
        'pagename': 'Голосование',
        'menu': get_menu_context(),
        'found': False,
    }

    voting_list = Voting.objects.filter(id=id)
    if voting_list:
        voting = voting_list[0]
        voting_items = VotingItem.objects.filter(voting=voting)

        context['voting'] = voting
        context['found'] = True
        context['can_be_edited'] = can_be_edited(voting)

        if request.method == "POST":
            if 'vote_submit' in request.POST:
                if 'vote' in request.POST:
                    vote = dict(request.POST)['vote']
                    if vote:
                        for v in vote:  # 'option'
                            print(repr(voting_items), repr(vote), repr(v))
                            FactOfVoting.objects.create(voting_item=voting_items[int(v[6:])], user=request.user, date=timezone.now())

                        vote_notification(request.user, voting)
                        check_voting(voting)

                        messages.info(request, 'Ваш голос успешно зачтён.')

                        return redirect(voting.get_url())
            elif 'remove_submit' in request.POST:
                voting.delete()
                messages.error(request, 'Голосование успешно удалено.')
                return redirect(reverse('voting.list'))

        context['form'] = get_vote_form(voting, request.user)
        context['form_type_text'] = voting_items[0].get_type()

        # context['icon_info'] = get_icon_html('info', 24)
        # context['icon_vote'] = get_icon_html('check-' + 'box' if voting.type == 1 else 'circle', 24)
        # context['icon_chart'] = get_icon_html('pie-chart', 24)
        # context['icon_context'] = get_icon_html('document-text', 24)
        # context['icon_edit'] = get_icon_html('pencil', 24)
        # context['icon_report'] = get_icon_html('alert-triangle', 24)

        context['chart_labels'] = ["#{}".format(i + 1) for i in range(len(voting_items))]
        context['chart_values'] = [len(FactOfVoting.objects.filter(voting_item=item)) for item in voting_items]

        # context['voting_items'] = voting_items
        # context['is_checked'] = [bool(FactOfVoting.objects.filter(user=request.user, voting_item=item)) for item in voting_items]
        # context['already_voted'] = any(context['is_checked'])
        # context['voting_items_ziped'] = zip(voting_items, context['is_checked'])

        # can_see_author = False
        # if voting.author_visible == 0:
        #     can_see_author = True
        # elif voting.author_visible == 1 and request.user.is_authenticated:
        #     can_see_author = True
        # elif voting.author == request.user or request.user.is_staff:
        #     can_see_author = True
        #
        # can_see_voting_users = False
        # if voting.voting_users_visible == 0:
        #     can_see_voting_users = True
        # elif voting.voting_users_visible == 1 and request.user.is_authenticated:
        #     can_see_voting_users = True
        # elif voting.author == request.user or request.user.is_staff:
        #     can_see_voting_users = True
        #
        # can_see_results = False
        # if voting.result_visible == 0:
        #     can_see_results = True
        # elif voting.result_visible == 1 and request.user.is_authenticated:
        #     can_see_results = True
        # elif voting.author == request.user or request.user.is_staff:
        #     can_see_results = True
        #
        # context['can_see_author'] = can_see_author
        # context['can_see_voting_users'] = can_see_voting_users
        # context['can_see_results'] = can_see_results

    return render(request, join('pages', 'voting-current.html'), context)


@login_required
def new_voting_page(request):
    # """
    # /new_voting
    # Страница с меню выбора типа голосования: бинарное, радио или комбо. Меню - форма с тремя радио-кнопками
    # и полем ввода количества вариантов. При нажатии на submit отправляется GET-запрос странице
    # /create_voting; тип голосования хранится в request.GET['type'].
    # """
    context = {
        'pagename': 'Новое голосование',
        'menu': get_menu_context(),
        'type_selected': False,
        'need_formset': True,
    }

    ALLOWED_TYPES = BINARY, RADIO, COMBO = 'binary', 'radio', 'combo'

    if request.method == "GET":
        if 'type' in request.GET:
            voting_type = request.GET['type']
            if voting_type in ALLOWED_TYPES:
                voting_form = NewVotingForm()
                context['voting_form'] = voting_form
                context['type_selected'] = True
                context['voting_type'] = voting_type

                formset = VotingItemFormset(initial=[{'text': ''}, {'text': ''}])
                context['formset'] = formset
                context['need_formset'] = voting_type != BINARY
                # VotingItemForm
    elif request.method == "POST":  # and 'type' in request.POST and request.POST['type'] in ALLOWED_TYPES:
        voting_type = request.POST['type']
        voting_form = NewVotingForm(request.POST or None)
        context['voting_form'] = voting_form
        context['type_selected'] = True
        context['need_formset'] = voting_type != BINARY

        formset = VotingItemFormset(request.POST or None)
        context['formset'] = formset
        if voting_form.is_valid() and formset.is_valid():
            voting = voting_form.save(commit=False)

            voting.author = request.user
            voting.status = 1
            voting.date = timezone.now()
            voting.save()

            for form in formset:
                text = form.cleaned_data.get('text')
                if text:
                    VotingItem(type=int(voting_type == COMBO), voting=voting, text=text).save()

            new_voting_notification(voting)

            messages.success(request, 'Новое голосование успешно создано.')

            return redirect(reverse('voting.list'))

    return render(request, join('pages', 'voting-new.html'), context)


@login_required
def new_report_page(request):
    context = {
        'pagename': 'Новая жалоба',
        'menu': get_menu_context (),
    }

    report_form = None
    voting_id = 0

    if request.method == "GET":
        if 'voting_id' in request.GET:
            voting_id = int(request.GET['voting_id'])
            context['voting'] = (list(Voting.objects.filter(id=voting_id)) + [None])[0]
            report_form = NewReportForm()
    elif request.method == "POST" and 'voting_id' in request.POST:  # and 'type' in request.POST and request.POST['type'] in ALLOWED_TYPES:
        report_form = NewReportForm(request.POST or None)
        if report_form.is_valid():
            report = report_form.save(commit=False)
            voting_id = int(request.POST['voting_id'])
            report.user = request.user
            report.voting = Voting.objects.get(pk=voting_id)
            report.status = 0
            report.date = timezone.now()

            report.save()

            messages.warning(request, 'Новая жалоба отправлена на рассмотрение администрации.')

        return redirect(reverse('voting.list'))

    context['form'] = report_form
    context['voting_id'] = voting_id

    return render(request, join('pages', 'report-new.html'), context)


@login_required
def edit_voting_page(request, id):
    context = {
        'pagename': 'Голосование не найдено',
        'menu': get_menu_context(),
        'found': False,
    }

    voting_list = Voting.objects.filter(id=id)
    if voting_list:
        voting = voting_list[0]
        voting_items = VotingItem.objects.filter(voting=voting)
        context['pagename'] = "Редактирование голосования"
        context['voting'] = voting
        context['found'] = True
        context['can_be_edited'] = can_be_edited(voting)

        context['voting_type'] = voting.type

        voting_form = NewVotingForm(instance=voting)
        formset = VotingItemFormset(initial=[{'text': vi.text} for vi in voting_items])

        if request.method == "POST" and context['can_be_edited']:  # and 'type' in request.POST and request.POST['type'] in ALLOWED_TYPES:
            voting_form = NewVotingForm(request.POST or None)
            formset = VotingItemFormset(request.POST or None)

            if voting_form.is_valid() and formset.is_valid():
                voting.title = voting_form.cleaned_data.get('title')
                voting.text = voting_form.cleaned_data.get('text')
                voting.maximum_votes = voting_form.cleaned_data.get('maximum_votes')
                voting.status = 1
                voting.date = timezone.now()
                voting.save()
                voting_type = voting.type

                for v in voting_items:
                    v.delete()

                for form in formset:
                    text = form.cleaned_data.get('text')
                    if text:
                        VotingItem.objects.create(type=voting_type, voting=voting, text=text)

                messages.success(request, "Голосование успешно изменено.")

                return redirect(voting.get_url())

        context['voting_form'] = voting_form
        context['formset'] = formset

    return render(request, join('pages', 'voting-edit.html'), context)
