import os


def get_icon_html(icon, size=32, id='', cls='', style=''):
    icon_path = os.path.join('/static', 'images', 'icons', icon + '.svg')
    return """<img src="{path}" id="{id}" alt="" width="{size}" height="{size}" class="{cls}" style="{style}">""".format(
        path=icon_path, id=id, size=size, style=style, cls=cls)


# class GetIconObject:
#     def __getattr__(self, item: str):
#         item = item.replace('_', '-')
#         print(item)
#         if item[-1].isdigit():
#             num_len = 1
#             while item[-num_len].isdigit():
#                 num_len += 1
#             return get_icon_html(item[: -num_len], int(item[-num_len + 1:]))
#         return get_icon_html(item)


def truncate(s, max_len=25):
    if max_len <= 0: return s
    return (s[:max_len] + '...') if len(s) > max_len else s
